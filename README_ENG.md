# Padi AI

## What is Padi AI? ##
Padi AI is an application for using cameras on mobile phones to detect the occurrence of bacterial blight, brown spot and rice blast on rice paddy leaves. The app is made for the [Division of Agricultural Engineering, Department of Agriculture, Malaysia](http://pertanian.selangor.gov.my/). 

Detection is accomplished using a custom trained model with Tensorflow, then the trained model is optimized for size and performance to fit into an Android APK. 

Padi AI is still in development and placed here for testing purposes. If you would like to try it out you can download the latest Android APK here:

[**-->Download Padi AI**](https://docs.google.com/forms/d/e/1FAIpQLSdhvMm8Wgpjn_JPThMB6k41NUnMYyyj9FyT5P0cZXtLpGGf-Q/viewform?usp=sf_link)

<img src="/Screenshot/4live.jpg" width="45%" height="45%"> <img src="/Screenshot/5live.jpg" width="45%" height="45%"><br/>

Image inferencing is done on the edge, thus enabling the app to be used without an internet connection. This also means data privacy is maintained as no data is transmited out. The training dataset was by [aldrin233](https://github.com/aldrin233/RiceDiseases-DataSet), which was further manually annotated, augmented, and pre-processed. Several models were trained and tuned to optimize for performance on mobile phones. 

<img src="/Screenshot/1brownspot.jpg" width="30%" height="30%"> <img src="/Screenshot/2bacterialblight.jpg" width="30%" height="30%"> <img src="/Screenshot/3riceblast.jpg" width="30%" height="30%"><br/>

## System Requirements ##
- Android mobile phone with a built-in camera.
- OS  : Android 6.0 (Marshmallow) or above
- CPU : Quad-core 1.2 GHz (Minimum)
- RAM : 2GB RAM (Recommended 6GB RAM)
- Minimum 200 MB free storage (Internal Storage Recommended)

## How To Install ##
**Step 1** <br/>
Enable "Unknown sources" in Setting -> Security -> switch "Unknown Sources" to "On".

<img src="/Screenshot/Screenshot_20200219-191727.jpg" width="45%" height="45%"><img src="/Screenshot/Screenshot_20200219-191734.jpg" width="45%" height="45%"><br/>

*Different phone models or Android versions might differ.*

**Step 2** <br/>
Download the latest Padi AI APK file: [**-->Download Padi AI**](https://docs.google.com/forms/d/e/1FAIpQLSdhvMm8Wgpjn_JPThMB6k41NUnMYyyj9FyT5P0cZXtLpGGf-Q/viewform?usp=sf_link) <br/>

**Step 3** <br/>
Open the downloaded APK using any File Manager application or just select from the download list. <br/>

**Step 4** <br/>
Once the APK file is open, a permission message will pop-out as below. Agree to install. <br/>

<img src="/Screenshot/Screenshot_20200220-112528.jpg" width="45%" height="45%"><img src="/Screenshot/Screenshot_20200220-112532.jpg" width="45%" height="45%"><br/>

**Step 5** <br/>
After installation is complete, a notification windows will pop-out as below and the application is ready to launch by pressing "Open". <br/>

Upon launching the app, detection will start immediately. <br/>

<img src="/Screenshot/Screenshot_20200220-112624.jpg" width="30%" height="30%">  <img src="/Screenshot/5live.jpg" width="60%" height="60%"><br/>

## Test by pointing to a diseased paddy leaf ##

Note: detection works best on actual plants, or by colour printing the images. 

<img src="/Screenshot/test_brownspot.jpg" width="45%" height="100%"> <img src="/Screenshot/test_riceblast.jpg" width="45%" height="100%">