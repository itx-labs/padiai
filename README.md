# Padi AI

## Apa itu Padi AI? ##
Padi AI ialah suatu aplikasi yang menggunakan kamera di telefon bimbit untuk mengesan keberadaan hawa bakteria, tompokan coklat, dan ledakan beras di daun-daun padi beras. Aplikasi ini dicipta untuk [Bahagian Kejuruteraan Pertanian, Jabatan Pertanian, Malaysia](http://pertanian.selangor.gov.my/). 

Pengesanan ini boleh dicapai menggunakan model terlatih iaitu Tensorflow, kemudian model terlatih ini dioptimum untuk memuatkan saiz dan pencapaian dalam Andriod APK.

Padi AI masih berada dalam proses kajian dan diletakkan di sini untuk tujuan latihan. Jika anda ingin menggunakannya, anda boleh muat turun Android APK yang terbaru di sini:

[**-->Muat turun Padi AI**](https://docs.google.com/forms/d/e/1FAIpQLSdhvMm8Wgpjn_JPThMB6k41NUnMYyyj9FyT5P0cZXtLpGGf-Q/viewform?usp=sf_link)

<img src="/Screenshot/4live.jpg" width="45%" height="45%"> <img src="/Screenshot/5live.jpg" width="45%" height="45%"><br/>

Imej inferensi boleh dilakukan di aplikasi ini walau tanpa sambungan internet. Ini juga bermakna data privasi kekal terpelihara kerana tiada data yang dipindahkan keluar. Latihan set data dilaksanakan oleh [aldrin233](https://github.com/aldrin233/RiceDiseases-DataSet), kemudian catatan dilakukan, diperkukuh dan dipraproses secara manual. Beberapa model telah dilatih untuk mengoptimumkan pencapaian di telefon bimbit.

<img src="/Screenshot/1brownspot.jpg" width="30%" height="30%"> <img src="/Screenshot/2bacterialblight.jpg" width="30%" height="30%"> <img src="/Screenshot/3riceblast.jpg" width="30%" height="30%"><br/>

## Keperluan Sistem ##
- Telefon bimbit android yang berkamera
- OS  : Android 6.0 (Marshmallow) atau ke atas
- CPU : Quad-core 1.2 GHz (Minima)
- RAM : 2GB RAM (Rekomendasi 6GB RAM)
- Minima 200 MB simpanan percuma (Rekomendasi simpanan dalaman)

## Cara untuk pasang ##
**Langkah 1** <br/>
Aktifkan “Sumber tidak diketahui” di Tatapan -> Keselamatan -> tukar “Sumber tidak diketahui” kepada “hidup”

<img src="/Screenshot/Screenshot_20200219-191727.jpg" width="45%" height="45%"><img src="/Screenshot/Screenshot_20200219-191734.jpg" width="45%" height="45%"><br/>

*Telefon bimbit yang berlainan models atau Android berkemungkinan mempunyai versi yang tidak sama.*

**Langkah 2 2** <br/>
Muat turun fail Padi AI APK yang terbaru. Muat turun Padi AI: [**-->Download Padi AI**](https://docs.google.com/forms/d/e/1FAIpQLSdhvMm8Wgpjn_JPThMB6k41NUnMYyyj9FyT5P0cZXtLpGGf-Q/viewform?usp=sf_link) <br/>

**Langkah 3** <br/>
Buka APK yang telah dimuat turun menggunakan mana-mana aplikasi Pengurusan Fail atau terus pilih daripada senarai yang telah dimuat turun.<br/>

**Langkah 4** <br/>
Setelah fail APK dibuka, satu mesej kebenaran akan keluar seperti berikut. Setuju untuk pemasangan. <br/>

<img src="/Screenshot/Screenshot_20200220-112528.jpg" width="45%" height="45%"><img src="/Screenshot/Screenshot_20200220-112532.jpg" width="45%" height="45%"><br/>

**Langkah 5** <br/>
Setelah pemasangan selesai, satu notifikasi akan keluar seperti di bawah dan aplikasi ini sedia untuk dilancarkan dengan menekan “Buka”. <br/>

Sebaik sahaja aplikasi ini dilancarkan, pengesanan akan dimulakan serta-merta. <br/>

<img src="/Screenshot/Screenshot_20200220-112624.jpg" width="30%" height="30%">  <img src="/Screenshot/5live.jpg" width="60%" height="60%"><br/>

## Ujian dilakukan pada daun padi yang berpenyakit ##

Nota: Pengesanan yang terbaik perlulah dilakukan atas tumbuh-tumbuhan yang sebenar, atau pada cetakan imej yang berwarna. 

<img src="/Screenshot/test_brownspot.jpg" width="45%" height="100%"> <img src="/Screenshot/test_riceblast.jpg" width="45%" height="100%">